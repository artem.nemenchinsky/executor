unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, IniFiles, Process, LCLType;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Process1: TProcess;
    Process2: TProcess;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  public
    appTitle: String;
    importButtonTitle: String;
    importButtonCommand: String;
    importButtonCommandArg1: String;
    importButtonCommandArg2: String;
    importCommandRunHidden: Boolean;
    exportButtonTitle: String;
    exportButtonCommand: String;
    exportButtonCommandArg1: String;
    exportButtonCommandArg2: String;
    exportCommandRunHidden: Boolean;
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
var
  iniFileName: String;
  iniFile: TINIFile;
begin
  importCommandRunHidden := false;
  exportCommandRunHidden := false;
  iniFileName := ExtractFilePath(Application.ExeName) + 'executor.ini';
  iniFile := TINIFile.Create(iniFileName);
  try
    appTitle := iniFile.ReadString('Application','AppTitle','Import/export launcher');
    self.Caption := appTitle;
    importButtonTitle := iniFile.ReadString('ImportButton','ButtonName','Import');
    importButtonCommand := iniFile.ReadString('ImportButton','ButtonCmd','import.exe');
    importButtonCommandArg1 := iniFile.ReadString('ImportButton','ButtonCmdArg1','');
    importButtonCommandArg2 := iniFile.ReadString('ImportButton','ButtonCmdArg2','');
    importCommandRunHidden := UpperCase(iniFile.ReadString('ImportButton','RunHidden','N')) = 'Y';
    exportButtonTitle := iniFile.ReadString('ExportButton','ButtonName','Export');
    exportButtonCommand := iniFile.ReadString('ExportButton','ButtonCmd','import.exe');
    exportButtonCommandArg1 := iniFile.ReadString('ExportButton','ButtonCmdArg1','');
    exportButtonCommandArg2 := iniFile.ReadString('ExportButton','ButtonCmdArg2','');
    exportCommandRunHidden := UpperCase(iniFile.ReadString('ExportButton','RunHidden','N')) = 'Y';
    Button1.Caption := importButtonTitle;
    Button2.Caption := exportButtonTitle;
    Process1 := TProcess.Create(nil);
    Process2 := TProcess.Create(nil);
    Process1.Executable := importButtonCommand;
    Process1.Parameters.Add(importButtonCommandArg1);
    Process1.Parameters.Add(importButtonCommandArg2);
    Process1.Options := [poWaitOnExit];
    if importCommandRunHidden then
    begin
      Process1.ShowWindow := swoHIDE;
    end;
    Process2.Executable := exportButtonCommand;
    Process2.Parameters.Add(exportButtonCommandArg1);
    Process2.Parameters.Add(exportButtonCommandArg2);
    Process2.Options := [poWaitOnExit];
    if exportCommandRunHidden then
    begin
      Process2.ShowWindow := swoHIDE;
    end;
  finally
    iniFile.Free;
  end;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  Process1.Execute;
  Application.MessageBox('Execution completed', 'Execution result', MB_OK);
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Process2.Execute;
  Application.MessageBox('Execution completed', 'Execution result', MB_OK);
end;

end.

